import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { motion } from "framer-motion"
import axios from "axios";
import queryString from "query-string";

import SearchCard from "../../components/SearchCards/SearchCards";
import DummyImage from "../../assets/photo-1585974738771-84483dd9f89f.jfif";

import "./HomePage.style.css";

const HomePage = (props) => {
  const [data, setData] = useState("");
  const [results, setResults] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    let elmnt = document.getElementsByClassName("result")[0];
    let value = queryString.parse(props.history.location.search);
   if(value.q) {
      setIsLoading(true);
      axios
      .get(
        `https://13nhuqce9a.execute-api.ap-southeast-1.amazonaws.com/stag?q=${value.q}`
      )
      .then((res) => {
        setResults(res.data.hits.hit);
        setIsLoading(false);
        if (results && results.length > 0) {
          elmnt.scrollIntoView();
        }
      })
      .catch((err) => {
        setResults(null);
        setIsLoading(false);
        console.log(err)
      });
    } else {
      props.history.push(`/?q=zoom`);
    }
  }, [props.history.location.search, props.history]);

  const onSubmit = (event) => {
    let elmnt = document.getElementsByClassName("result")[0];
    event.preventDefault();
    props.history.push(`/?q=${data}`);
    elmnt.scrollIntoView();
  };

  const handleChange = (event) => {
    setData(event.target.value);
  };

  const renderResults = (results) => {
    if (results && results.length) {
      return results.map((item) => (
        <SearchCard item={item} key={item.id} />
      ));
    } else {
      return (
        <div className="col-12 mt-5">
          <h1>No Results</h1>
        </div>
      );
    }
  };

  const topFunction = () => {
    let elmnt = document.getElementsByClassName("jumbotron")[0];
    if (results && results.length > 0) {
      elmnt.scrollIntoView();
    }
  };

  return (
    <React.Fragment>
      <motion.button 
       whileHover={{scale: 1.2}}
       whileTap={{ scale: 0.9 }}
       onClick={topFunction} 
       id="myBtn" 
       title="Go to top"
      >
        Top
      </motion.button>
      <div>
        <div
          className="jumbotron"
          style={{
            background: `linear-gradient( rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) ), url(${DummyImage})`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            minHeight: "100vh",
            borderRadius:"0px"
          }}
        >
          <a
            href="https://superpro.ai/"
            className="btn btn-outline-primary btn-lg mx-4"
          >
            Become a Super<b>pro</b>
          </a>
          <div className="container mt-5">
              <div>
                <div className="text-center mb-5">
                  <div className="d-sm-none d-md-block">
                  <h1 className="display-2 text-white d-none d-sm-block ">
                    <b>If it’s happening on zoom, you will find it here.</b>
                  </h1>
                  </div>
                  <h3 className="text-white d-none d-sm-block d-md-none" style={{fontFamily:"Arial, Helvetica, sans-serif"}}>
                  If it’s happening on zoom, you will find it here.
                  </h3>
                  <h3 className="text-white d-none d-block d-sm-none" style={{fontFamily:"Arial, Helvetica, sans-serif"}}>
                  If it’s happening on zoom, you will find it here.
                  </h3>
                </div>
              </div>
            <div className="text-right">
              <form id="form1" onSubmit={onSubmit}>
                <div className="input-group">
                  <input
                    style={{ background: "transparent" }}
                    type="text"
                    className="form-control p-4  text-white"
                    placeholder="Search Now"
                    aria-label="Recipient's username"
                    aria-describedby="basic-addon2"
                    name="search"
                    onChange={handleChange}
                    required
                  />
                  <div className="input-group-append">
                    <button
                      className="input-group-text btn btn-danger py-2 px-4"
                      type="submit"
                      form="form1"
                      value="Submit"
                    >
                      <span id="basic-addon2">
                        <i className="fa fa-search" aria-hidden="true"></i>
                      </span>
                    </button>
                  </div>
                </div>
              </form>
            </div>
            <div className="text-white mt-5 d-none d-sm-block" style={{fontFamily:"Arial, Helvetica, sans-serif"}}>
              <h3 className="my-3 text-center">Looking for zoominars or online events to attend?</h3>
              <h3 className="my-3 text-center">Wondering how you can find a webinar or online event to attend across the world? </h3>
              <h3 className="my-3 text-center">Every 24 hours you will find the new Zoom from all around the world listed here. </h3>
            </div>
            <div className="text-white mt-5 d-block d-sm-none" style={{fontFamily:"Arial, Helvetica, sans-serif"}}>
              <h6 className="my-3 text-center">Looking for zoominars or online events to attend?</h6>
              <h6 className="my-3 text-center">Wondering how you can find a webinar or online event to attend across the world? </h6>
              <h6 className="my-3 text-center">Every 24 hours you will find the new Zoom from all around the world listed here. </h6>
            </div>
          </div>
        </div>
      </div>
      <div className="container result mb-5">
        <div className="row text-center">
          {isLoading ? (
            <div className="col-12 mt-5">
              <div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
              </div>
            </div>
          ) : (
            renderResults(results)
          )}
        </div>
      </div>
    </React.Fragment>
  );
};

export default withRouter(HomePage);
