import React from 'react';
import Moment from 'react-moment';
import { motion } from "framer-motion"

export default function SearchCards(props) {
   return (
      <motion.div 
       whileHover={{scale: 1.07}}
       whileTap={{ scale: 0.9 }}
       className="col-md-6 col-12 mt-4 text-left" 
       id={props.item.id}
      >
          <a
            href={props.item.fields.link}
            style={{ color: "black", textDecoration: "none" }}
          >
            <div className="card shadow-lg h-100" style={{ width: "100%", borderRadius: "10px" }}>
              <div className="card-body">
                <h5 className="card-title">{props.item.fields.title}</h5>
                <h6 className="card-subtitle mb-2 text-muted">
                <Moment format='MMMM Do YYYY, h:mm a'>{props.item.fields.date}</Moment>
                </h6>
                <p className="card-text">{props.item.fields.snippet}</p>
                <span className="card-link">
                  {props.item.fields.displayed_link}
                </span>
                <br></br>
                <span className="card-link">
                  {props.item.fields.link}
                </span>
              </div>
            </div>
          </a>
      </motion.div>
   )
}
