import React, {useEffect} from "react";
import { Route, Switch } from "react-router-dom";

import HomePage from "./pages/HomePage/HomePage";

import "bootstrap/dist/css/bootstrap.min.css";

const App = () => {
  useEffect(() => {
    if(process.env.NODE_ENV !== "development"){
      if (window.location.protocol !== 'https:') {
        window.location.href = window.location.href.replace("http://", "https://");
      }
    }
  }, [])
  return (
    <React.Fragment>
      <Switch>
        <Route exact path="/" component={HomePage} />
      </Switch>
    </React.Fragment>
  );
};
export default App;
